var height = window.innerHeight;

window.addEventListener("resize", function() {
    var page = $(".ui-page-active");
    var inputField = page.find("#inputCircle");
    if (window.innerHeight < height) {
        inputField.addClass("input-area-active");
    } else if (window.innerHeight >= height) {
        inputField.removeClass("input-area-active");
    }
});
