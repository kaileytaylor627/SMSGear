var ProviderAppName = "SMSGearProvider";
var CHANNEL = 123;
var SAAgent = null;
var SASocket = null;

var conversationsObj = null;
var lastClickedConversationId = -1;

var conversations_page = document.getElementById('page_conversations');
var conversations_loading_page = document.getElementById('page_loading_conversations');
var conversation_messages_page = document.getElementById('page_conversation_messages');
var reply_message_page = document.getElementById('page_reply_message');

var loading_conversations_text = document.getElementById('loading_conversations_text');

(function() {
    window.addEventListener('tizenhwkey', function(ev) {
        if (ev.keyName === "back") {
            var page = document.getElementsByClassName('ui-page-active')[0];
            if (page) {
                if (page.id === "page_conversations" || page.id === "page_loading_conversations") {
                    try {
                        tizen.application.getCurrentApplication().exit();
                    } catch (ignore) {}
                } else {
                    window.history.back();
                }
            }
        }
    });
}());

function sendSMS() {
    var input = $(".ui-page-active").find("#inputCircle");

    SASocket.sendData(CHANNEL, "sendSMS " + lastClickedConversationId + " " + input.val());

    input.val('');

    loading_conversations_text.innerHTML = 'Sending...';
    tau.changePage(conversations_loading_page);
}

function on_reply_conversation_clicked() {
    tau.changePage(reply_message_page);
}

function on_conversation_clicked(conversationId) {
    var conversation_messages = document.getElementById('conversation_messages');
    conversation_messages.innerHTML = '';

    for (var i = 0; i < conversationsObj.length; i++) {
        if (conversationsObj[i].conversationId === conversationId) {
            for (var j = conversationsObj[i].messages.length - 1; j >= 0; j--) {
                var li = document.createElement('li');
                var talkBubble = document.createElement('div');
                var talkText = document.createElement('div');
                var paragraph = document.createElement('p');

                li.appendChild(talkBubble);
                talkBubble.appendChild(talkText);
                talkText.appendChild(paragraph);

                var classes = '';
                if (conversationsObj[i].messages[j].type === 1) {
                    classes = 'talk-bubble tri-right left-top';
                } else if (conversationsObj[i].messages[j].type === 2) {
                    classes = 'reply-bubble tri-right right-top';
                }

                li.setAttribute('class', 'conversations-li');
                talkBubble.setAttribute('class', classes);
                talkText.setAttribute('class', 'talktext');
                paragraph.innerHTML = conversationsObj[i].messages[j].message;

                conversation_messages.appendChild(li);
            }
            break;
        }
    }
    lastClickedConversationId = conversationId;

    tau.changePage(conversation_messages_page);
    scrollToBottom(conversation_messages_page);
}

function onReceiveCallback(channelId, data) {
    console.log("Received message from the [" + channelId + "] ! Length : " + data.length);

    if (data.lastIndexOf('[', 0) === 0) {
        conversationsObj = JSON.parse(data);

        var conversations_list = document.getElementById('conversations_list');
        conversations_list.innerHTML = '';
        for (var i = 0; i < conversationsObj.length; i++) {
            var contactName = conversationsObj[i].contactName;
            if (contactName === undefined) {
                contactName = conversationsObj[i].phoneNumber;
            }

            var newItem = document.createElement('li');
            var a = document.createElement('a');
            var div1 = document.createElement('div');
            var div2 = document.createElement('div');
            var div3 = document.createElement('div');
            a.setAttribute('href', '#');
            newItem.appendChild(a);

            newItem.setAttribute('onclick', 'on_conversation_clicked(' + conversationsObj[i].conversationId + ')');
            div1.setAttribute('class', 'conversations_contact_name');
            div2.setAttribute('class', 'conversations_preview');
            div3.setAttribute('class', 'conversations_time');

            div1.innerHTML = contactName;
            div2.innerHTML = conversationsObj[i].lastMessage;
            var lastMsgDate = new Date(conversationsObj[i].lastMessageDate);
            if (lastMsgDate.toLocaleDateString() == new Date().toLocaleDateString()) {
                var timeSplitted = lastMsgDate.toLocaleTimeString().split(':');
                div3.innerHTML = timeSplitted[0] + ":" + timeSplitted[1];
            } else {
                var timeSplitted = lastMsgDate.toLocaleString().split(':');
                div3.innerHTML = timeSplitted[0] + ":" + timeSplitted[1];
            }

            a.appendChild(div1);
            a.appendChild(div2);
            a.appendChild(div3);

            conversations_list.appendChild(newItem);
        }
        
        tau.changePage(conversations_page);
    } else {
        console.log("Is not json");
    }
    document.dispatchEvent(new CustomEvent("updatesnaplist"));
}

var connectToAgentCallback = {
    onconnect: function(socket) {
        console.log('Connected !');
        SASocket = socket;
        SASocket.setDataReceiveListener(onReceiveCallback);
        SASocket.setSocketStatusListener(function(reason) {
            console.log("Service connection lost, Reason : [" + reason + "]");
            disconnect();
        });

        console.log('Requesting SMS');
        loading_conversations_text.innerHTML = 'Requesting Conversations...';
        SASocket.sendData(CHANNEL, "getSMSConversations");
    },
    onerror: function(err) {
        console.log("Failed to connect to agent : [" + err.name + "]: [" + err.message + "]");
    }
};

var peerAgentFindCallback = {
    onpeeragentfound: function(peerAgent) {
        try {
            console.log("Peer agent found : " + peerAgent.appName);
            if (peerAgent.appName === ProviderAppName) {
                console.log('Sending connecting request.');
                SAAgent.setServiceConnectionListener(connectToAgentCallback);
                SAAgent.requestServiceConnection(peerAgent);
            } else {
                alert('Expected app : ' + ProviderAppName + ' but found : ' + peerAgent.appName);
            }
        } catch (err) {
            console.log("exception (onpeeragentfound)[" + err.name + "] msg[" + err.message + "]");
        }
    },
    onerror: function(err) {
        console.log("Error while searching for Peer Agent [" + err + "]");
    }
};

function on_SAAgent_initialized(agents) {
    try {
        if (agents.length > 0) {
            SAAgent = agents[0];

            SAAgent.setPeerAgentFindListener(peerAgentFindCallback);
            SAAgent.findPeerAgents();
            console.log("Searching for peer agents...");
        } else {
            console.log("Error : No SAAgent found.");
        }
    } catch (err) {
        console.log("Find Peer Agents exception [" + err.name + "]: [" + err.message + "]");
    }
}

function connect() {
    console.log("Connecting...");
    if (SASocket) {
        console.log('Already connected!');
        return false;
    }
    try {
        loading_conversations_text.innerHTML = 'Connecting to phone...';
        webapis.sa.requestSAAgent(on_SAAgent_initialized, function(err) {
            console.log("Request SAAgent failed [" + err.name + "]: [" + err.message + "]");
        });
    } catch (err) {
        console.log("Request SAAgent exception [" + err.name + "]: [" + err.message + "]");
    }
}

function disconnect() {
    try {
        if (SASocket !== null) {
            SASocket.close();
            SASocket = null;
            console.log("closeConnection");
        }
    } catch (err) {
        console.log("exception [" + err.name + "] msg[" + err.message + "]");
    }
}

connect();
